# Let's Cook - Projektbericht

<!--- Deckblatt wird mit MS Word erstellt --->

## Teammitglieder

Name | Matrikelnummer | EMail-Adresse
---- | -------------- | -------------
Linus Baudiss | 3511291 | baudissli78853@th-nuernberg.de
Fabian Besser | 3511431 | besserfa78860@th-nuernberg.de
Florian Füller | 3522329 | fuellerfl79613@th-nuernberg.de
Viktoria Galt | 3281189 | galtvi75978@th-nuernberg.de
Lukas Jakob | 3530964 | jakoblu80189@th-nuernberg.de
Manuel Kohler | 3512384 | kohlerma78973@th-nuernberg.de

## Inhaltsverzeichnis

<!--- Inhaltsverzeichnis wird mit MS Word erstellt --->

## Abkürzungsverzeichnis

Abkürzung | Erklärung
--------- | ---------
API | Application Programming Interface
apk | Android Package
CD | Continuous Delivery
CI | Continuous Integration
CI Kriterien | Corporate Identity Kriterien
DRY | do not repeat yourself
DTO | Data Transfer Objekt
GUI | Graphical User Interface
HEX | Hexadezimalsystem
HTTP | Hypertext Transfer Protocol
IDE | Integrated Development Environment
jar | Java Archive
JSON | JavaScript Object Notation
KISS | keep it simple stupid
MoSCoW | Must, Should, Could und Won't
MVP | Minimum Viable Product
RAM | Random-Access Memory
REST | Representational State Transfer
TDD | Test-First-Entwicklung
UI | User Interface
UML | Unified Modeling Language
URL |  Uniform Resource Locator
UTF | Universal Coded Character Set Transformation Format
xml | Extensible Markup Language
YAGNI | you ain‘t gonna need it
YAML | Yet Another Markup Language

## 1. Einleitung

### 1.1 Projektziel

Ziel der Projektarbeit ist die Entwicklung eines digitalen Kochbuches als Android Handy App, mit der der Nutzer eigene Rezepte erstellen kann.
Außerdem soll diese App eine Möglichkeit bieten, die bereits veröffentlichten Rezepte anderer Nutzer anzusehen.

Alle Rezepte werden zentral in einer Datenbank auf einem Server verwaltet.

## 2. Projektorganisation

### 2.1. Aufgabenverteilung

#### App-Frontend

GUI:

* Viktoria
* Manuel

API Anbindung:

* Lukas
* Fabian

#### App-Backend

Rest-API:

* Linus
* Florian

Die obige Übersicht beschreibt die grobe Aufgabenteilung im Team.
Dabei haben wir uns, ganz im Sinne der agilen Entwicklung, bei Problemen oder ungleicher Aufgabenverteilung gegenseitig unterstützt und geholfen um Ausfallzeiten so gering wie möglich zu halten.

#### 2.1.1 GUI

Wir haben uns mit sichtbaren Oberflächen (Views) sowie deren Funktionalität (Activitys) beschäftigt.
Vor dem Erstellen eines Layouts hat jeder von uns erstmal überlegt, wie es am besten zu designen ist.
Danach wurden die Ideen präsentiert und entschieden, wie die Oberfläche aussehen soll.
Bei gemeinsamen Dailys wurden die implementierten Oberflächen vorgestellt und eventuelle weitere Möglichkeiten bzw. Wünsche besprochen.
Es wurde darauf geachtet, dass der Nutzer eine möglichst komfortable und angenehme Erfahrung im Umgang mit unserer App hat.

#### 2.1.2 API-Anbindung

Hier haben wir uns um die Kommunikation zwischen App und Server gekümmert.
Dies beinhaltet das Erstellen von Model und Dto-Klassen, sowie das eigentliche Managen und Ausführen der Requests und das Verarbeiten der Antworten.
Dabei mussten wir einerseits ein einheitliches und praktisches Interface für das GUI-Team erstellen, wie auch eine einheitliche und persistente Kommunikation mit dem Server-Team vereinbaren.  

Wir haben uns hier oft im Pair getroffen, um ein Feature gemeinsam zu implementieren oder haben durch Approval-Regeln in Merge-Requests ein Code-Review durchgeführt.
Dabei musste ein anderer Entwickler die Änderungen im Quellcode ansehen, verstehen und dann beurteilen, ob diese Lösung perfekt zur Erfüllung des Issues passt.
Bei Unstimmigkeiten wurde diskutiert und dann die für beide beste Variante gewählt.
Wenn beide mit der Lösung zufrieden waren, wurden die Änderungen entsprechend gemerged (gelöst über das Branching-System von GitLab).

#### 2.1.3 Rest-API

Dieses Teilteam hat die für das Frontend ansprechbare REST Schnittstelle programmiert.
Somit können Rezept-Daten zwischen der App und dem Server ausgetauscht werden.
Hauptaufgaben des API-Teams war es, die REST Endpunkte und die entsprechenden Datenbankzugriffe zu implementieren.

### 2.2. Ressourcenplanung

Hardware

* Computer/Laptop
* Headset
* Kamera

Software

* Visual Studio Code
* Android Studio
* Intellij Ultimate Edition
* Postman
* Pivotal Web Services
* MongoDB
* GitLab
* Discord
* Zoom
* WhatsApp

### 2.3. Kostenplanung

Bei der Kostenplanung wurde darauf geachtet, dass die verwendete Software kostenfrei zur Verfügung steht oder die Technische Hochschule Ohm bereits Lizenzen für diese besitzt.
Die anfallenden Projektkosten sollen dadurch möglichst gering gehalten werden.

Die Kosten, die während der Projektdurchführung anfallen, werden durch die Cloud Native Platform Pivotal Web Services, auf der die REST-Api gehostet wird, verursacht.
Allerdings wird bei der erstmaligen Registrierung auf Pivotal Web Services ein *free trial credit* von 87$ zur Verfügung gestellt.

Wir hosten dort eine Instanz mit 1GB RAM, welche uns 21$ pro Monat kostet.
Bis zum Projektabschluss sind diese anfallenden Kosten also durch den Vorschusskredit gedeckt.
Wenn die App nach Projektabschluss weiter zur Verfügung stehen soll, müssen diese Kosten aus eigener Tasche gedeckt werden.

Die MongoDB Datenbank, welche auch auf dieser Plattform gehostet wird, ist gratis, da es eine Sandbox Version mit begrenztem Speicherplatz ist.
Sollten also in Zukunft zu viele Datenmengen in der Datenbank gespeichert werden, fallen auch hier noch zusätzliche Kosten an.

Für die Veröffentlichung der App bei Google Play benötigen wir einen Zugang zur Google Play Developer Console, die uns einmalig 25$ kosten würde.

### 2.4. Vorgehensmodell

Wir haben uns für ein agiles Vorgehensmodell entschieden und sozusagen die für uns passenden Methoden aus der Scrum Methode ausgewählt und umgesetzt.
Vor allem die drei folgenden Sätze aus dem agilen Manifest haben wir uns zu Herzen genommen und nach ihnen entwickelt:

1. Individuen und Interaktionen haben Vorrang vor Prozessen und Werkzeugen.
2. Funktionierende Software hat Vorrang vor umfangreicher Dokumentation.
3. Das Eingehen auf Änderungen hat Vorrang vor strikter Planverfolgung.

Folgende Kernprinzipien der agilen Software Entwicklung verfolgen und versuchen wir umzusetzen im Entwicklungsprozess:

1. **Inkrementelle Entwicklung**: </br> Frühe Auslieferung von Produkt-Inkrementen
2. **Offen für Änderungen**: </br>Anforderungsänderungen sind als erwartet anzusehen, </br> „Vorwegnahme von Wandel“ als ein elementares Prinzip
3. **Menschen statt Prozesse**: </br> Entwicklungsteams dürfen konkrete Vorgehensweise selbst festlegen, </br> keine engen Verfahrensvorschriften
4. **Einfachheit**: </br> komplizierte Elemente aktiv aus Produkt und Prozess entfernen, </br> kein Fokus aus Prozessdokumente

Bei der Entwicklung des Quellcodes sind wir dem *KISS*, *DRY* und *YAGNI* Prinzipen gefolgt.

Unsere Anforderungen verwalten wir mit Story Cards in einem digitalen Scrum Board. Diese werden, wenn nötig, inkrementell angepasst.
Wir versuchen unseren Quellcode test-driven zu programmieren, auch wenn es zugegebener Maßen nicht immer perfekt geklappt hat.
Dann wurden die notwendigen Unit Tests im Nachhinein ergänzt.
Programmiert wird normalerweise im Pair, mit einer Bildschirmübertragung über Zoom.
Dies hilft Bugs schon während der Entwicklung, durch das Vier-Augen-Prinzip, zu minimieren.
Außerdem hat man sozusagen immer ein direktes Code Review *on the fly*, da beide Entwickler sich gegenseitig auf gute Code Qualität abprüfen.
Nichtsdestotrotz wird vor jedem *merge* in den master Branch noch ein gesondertes Code Review mit dem gesamten Entwicklungsteam für den Teilbereich (Front-/Backend) abgehalten.
Bei diesem wird auf die Einhaltung der Definition of Done abgeprüft.

Folgende Scrum Artefakte haben wir in unseren Entwicklungsprozess aufgenommen:

1. Selbstorganisation und direkte Kommunikation
2. Inkrementelle Entwicklung in einwöchigen Sprints
3. Ein multidisziplinäres Entwicklungsteam mit sechs Personen, jeder bringt unterschiedliche Skills mit
4. Eine Art "Product Owner", der Verantwortung für das Projekt übernimmt und versucht das Team zu motivieren/strukturieren
5. Product und Sprint Backlogs mit Story Cards
6. Ein "Daily" Scrum Meeting jeweils montags, mittwochs und samstags
7. Ein Planning Meeting vor jedem Meilenstein
8. Team Reviews und Retrospektiven werden meist *on the fly* in den Dailys gemacht, wenn nötig

Auch fließen einige Verfahren des Extreme Programming in unseren Entwicklungsprozess ein, obwohl wir dieses nicht explizit verfolgen.

1. Inkrementelle Planung, durch eventuelle Anpassung der Anforderungen pro Inkrement
2. Kleine Releases von MVPs
3. Einfacher Entwurf, basierend auf den aktuell vorliegenden Anforderungen
4. Test-Driven-Development (TDD)
5. Ständiges Refactoring des Quellcodes

## 3. Projektdurchführung

### 3.1 Verwaltungstool GitLab

Wir haben uns bei der Source-Code-Verwaltung und Planung der Storys und UseCases für das Tool `Git` entschieden.
Wir brauchten ein Tool, das eine Versionsverwaltung der verschiedenen Entwicklungsstände beinhaltet und das Arbeiten mehrerer Teammitglieder am selben Projekt möglich macht.
Auf der Suche nach einem solchen Tool sind wir (auch aufgrund der Empfehlung des Professors und persönlicher sowie beruflicher Erfahrung) auf Git gestoßen.  

Anschließend verglichen wir die beiden Alternativen `GitHub` und `GitLab`.
Dabei wurde uns klar, dass nur `GitLab` eine CI|CD-Pipeline ermöglicht.
Da wir eine solche Pipeline zum automatischen Testen und Deployen unserer Anwendungen geplant hatten, fiel die Entscheidung auf diese Git-Variante.

Da es in `GitLab` möglich ist Issues zu verwalten und eigene Boards (z.B. im Scrum-Style) zu erstellen, wollten wir auch die Planung unserer Anforderungen, UseCases und Story-Cards über die Anwendung lösen.
Somit liegen Planung und Durchführung an einer zentralen Stelle.
Das erleichterte nicht nur die Projektdurchführung, sondern machte den Entwicklungsprozess auch für alle Mitglieder durchsichtiger und verständlicher.

Wir pflegten unsere zuvor gemeinsam definierten Anforderungen als Issues in `GitLab` ein und erweiterten die UseCases zu Story-Cards mit Akzeptanzkriterien, Tasks und Abhängigkeiten.
Zudem erstellten wir ein Scrum-Board auf dem alle Issues einsortiert wurden.
Anhand des Boards ist es möglich den aktuellen Stand der Anforderungen und die anstehenden Aufgaben, sowie die Prioritäten der Entwicklung abzulesen.
Eine genauere Beschreibung der Anforderungsplanung findet sich im nächsten Punkt.

### 3.2 Anforderungen

#### 3.2.1 Meilenstein "Kick-Off"

Anforderungen zur Projektvorbereitung:

|Anforderung   | funktional/nicht-funktional
|---|---|
| Es soll eine Definition of Done erarbeitet werden, in welcher klar festgelegt ist welche Qualitätsziele der Code erfüllen muss. | nicht funktional |
| Es soll ein digitales Scrum Board erstellt werden, in welchem alle Story Cards der "Let's Cook" App zu finden sind. | nicht funktional |
| Es soll ein Grundgerüst für die Projektdokumentation erstellt werden, welches durch jedes Teammitglied verändert werden kann und zentral verwaltet wird. Dieses wird, wegen der einfachen Versionierung und Handhabung, in Markdown verfasst. | nicht funktional |

#### 3.2.2 Meilenstein "Alpha"

Anforderungen zur Projektvorbereitung:

| Anforderung | funktional/nicht-funktional
|---|---|
| Es soll eine funktionierende backend build pipeline erstellt werden, um schnelle Releases und Codequalität gewährleisten zu können. | nicht funktional |
| Es soll eine funktionierende frontend build pipeline erstellt werden, um schnelle Releases und Codequalität gewährleisten zu können. | nicht funktional |
| Es sollen Testdaten, für die Befüllung der Datenbank, erstellt werden, damit mit diesen der erste GET Endpoint implementiert werden kann. | nicht funktional |

Anforderungen zur Anwendung:

| Anforderung | funktional/nicht-funktional
|---|---|---|
| Die App soll für den Anwender alle gespeicherten Rezepte aus der Datenbank laden und in einer Liste anzeigen. | funktional |
| Die App muss dem Anwender die Möglichkeit bieten eine Detailansicht für jedes Rezept abrufen zu können. | funktional |
| Die App soll dem Anwender die Möglichkeit bieten ein Titel, ein Bild und eine Kurzbeschreibung von jedem Rezept in der Rezept-Listenansicht zu sehen. | funktional |
| Die App soll alle vorhandenen Rezepte aus der Rezeptdatenbank schnell und reibungslos laden. | nicht-funktional |

#### 3.2.3 Meilenstein "Abschluss"

Anforderungen zur Anwendung:

| Anforderung | funktional/nicht-funktional
|---|---|
| Die App soll dem Anwender die Möglichkeit bieten Rezepte in der App erstellen zu können. | funktional |
| Die App muss dem Anwender die Möglichkeit bieten bereits erstellte und veröffentlichte Rezepte nachträglich ändern zu können. | funktional |
| Die App soll dem Anwender die Möglichkeit bieten bereits erstellte und veröffentlichte Rezepte nachträglich löschen zu können. | funktional |
| Die App soll dem Anwender die Möglichkeit bieten eine Startseite zu sehen, wenn er die App öffnet | funktional |
| Die App muss dem Anwender die Möglichkeit bieten sich in der Anwendung mit eigenem Namen registrieren zu können, um eigene Rezepte zu erstellen.  | funktional |
| Die App muss dem Anwender die Möglichkeit bieten sich in der App mit eigenem Nutzernamen anmelden zu können, um seine eigens erstellten Rezepte verwalten können.| funktional |
| Die App soll dem Anwender keine Möglichkeit bieten die Rezepte von anderen Nutzern zu ändern oder löschen. | nicht-funktional |

#### 3.2.4 Organisation der Anforderungen

Wir organisieren unsere ToDos in einem eigenen GitLab Organisations Projekt, in welchem unser Scrum-Board aufgebaut ist.
Story Cards werden mit GitLab Issues erstellt, welche mit *labels* getaggt werden, die Spalten des Boards.

#### 3.2.4.1 Aufbau des Scrum Boards

Das Scrum Board hat folgende Spalten, angelehnt an dem Vorschlag aus der Vorlesung.

Spaltenname | Bedeutung
----------- | ---------
ideas | Story Cards mit unklarer Realisierung
product backlog | Story Cards mit geplanter Realisierung
sprint backlog | Story Cards, welche in der nächsten Iteration realisiert werden
active | Story Cards, die grade bearbeitet werden
for review | Story Cards mit fertiger Implementierung (inklusive Tests und Doku), die auf Abnahme vom Team warten
done | Story Cards mit fertiger Realisierung

![Stand-Scrum-Board-vor-Meilenstein-Alpha](./img/Stand-Scrum-Board-vor-Meilenstein-Alpha.png)

Stand des Scrum-Boards vor dem ersten Meilenstein "Alpha".

![Stand-Scrum-Board-nach-Meilenstein-Abschluss](./img/Stand-Scrum-Board-nach-Meilenstein-Abschluss.png)

Stand des Scrum-Boards nach dem letzten Meilenstein "Abschluss".

#### 3.2.4.2 Aufbau der Story Cards

* Eindeutige Nummer / ID
* Kurzbezeichnung der Story Card (als Titel)
* Eine genaue Beschreibung der Story Card, die folgendes enthält:
  * Die User Story
  * Möglicherweise etwaige Vorbedingungen (z.B. Abhängigkeiten zu anderen Story Cards)
  * Tasks zur Abarbeitung
  * Priorität aus Kundensicht nach MoSCoW-Methode (findet sich auch in der Gewichtung wieder)
  * Risiko hinsichtlich Umsetzungserfolg , berechnet durch Riskioindex (genauer: Skript Anforderungsanalyse S. 64f.)
  * Aufwandsschätzung mit „Story Points“ (1 SP = 1 idealer Vollzeitarbeitstag)
  * Auflistung von Akzeptanzkriterien
* Einen Bearbeiter
* Den dazugehörigen Milestone
* Verschiedene *labels*, in unserem Fall ist das nur die momentane Spalte im Board
* Ein angestrebtes Fertigstellungsdatum
* Das Gewicht der Story (1 = "must have", 2 = "should have", 3 = "can have", 4 = "won't have")

##### Beispiel Story Cards

![Anzeige-aller-Rezepte-in-der-Datenbank-Story-Card](./img/Anzeige-aller-Rezepte-in-der-Datenbank-Story-Card.png)

![Implementierung-einer-Detailansicht-fuer-Rezepte](./img/Implementierung-einer-Detailansicht-fuer-Rezepte.png)

### 3.3. Entwurf

#### 3.3.1 Definition of Done

Um eine einheitliche Code Qualität zu gewährleisten haben wir am Anfang des Projektes eine Defintion of Done entworfen.
Bei jedem Code Review wurde geprüft, ob die entsprechenden Qualitätskriterien eingehalten wurden.

##### Akzeptanz Kriterien

* Es wurden alle Akzeptanzkriterien erfüllt.
* Die Änderungen wurden vom Team akzeptiert.
* Die Dokumentation wurde erweitert.

##### Code Kriterien

* Der Code wurde Refactored.
* Der gesamte Code muss für die Projekte im jeweiligen master branch liegen.
* Es müssen essenzielle Tests für den Code geschrieben worden sein.
* Essenzielle Methoden wurden ausreichend kommentiert.
* Eine lokale statische Codeanalyse wurde ausgeführt und ausgewertet.
* Es wurde ein Code Review durchgeführt.

##### CI Kriterien

* Alle Tests müssen durchgelaufen und erfolgreich sein.
* Die neue Version der App steht zum direkten Download bereit.
* Die neue Version der Api wurde auf Cloud Foundry deployed.

#### 3.3.2 Technologie-Stack Backend

##### 3.3.2.1 Platform as a Service / Cloud Foundry

Bei `Hosting` und `Continous Delivery` haben wir uns für Cloud Foundry, einer sogenannten `Platform as a Service` (=PaaS), entschieden. Cloud Foundry ist für uns eine Plattform, welche das Backend Projekt hostet, mit der wir den Webservice überwachen und der uns eine simple Möglichkeit einer Anbindung unserer MongoDB Datenbank bereitstellt. Es gibt mehrere Preismodelle, wobei dabei auf den Speicherbedarf geachtet wird, den wir für unser Projekt schnell und einfach anpassen konnten. Zu dem Entwickler `Pivotal Software` gehört neben Cloud Foundry auch noch das Spring Framework, weswegen die Integration der beiden Komponenten ineinander sehr einfach gestaltet war.

##### 3.3.2.2 Java Spring Boot Framework

Bei der Auswahl der Frameworks haben wir uns für `Spring Boot` entschieden, da wir durch vorherige Projekte damit schon Erfahrungen sammeln konnten. `Spring Boot` ist eine Erweiterung des Spring Frameworks, welche einen schnellen Einstieg, ohne viel Konfigurationsarbeit ermöglicht. Ein Punkt, der für uns sehr nützlich ist, da es uns dadurch möglich wurde, zügig mit dem Programmieren beginnen zu können. Sie ist in und auch für Java geschrieben und ermöglicht es beispielsweise, unkompliziert eine Verbindung zu einer Datenbank aufzubauen, Unit Tests zu schreiben und am wichtigsten für unser Projekt:
Einen RESTful Service zu programmieren. Spring Boot bringt außerdem den `Apache Tomcat` Server mit, mit dem wir das Projekt sowohl lokal testen, als auch auf dem Server laufen lassen können.

##### 3.3.2.3 MongoDB Datenbank als Datenhaltung

Als Datenbank haben wir uns für `MongoDB` entschieden. MongoDB ist eine dokumentenorientierte `NoSQL-Datenbank`, das hat für uns einige entscheidende Vorteile. Wir können dadurch beispielsweise unsere JSON Dateien 1:1 in die Datenbank übernehmen, ohne dass noch ein Parser alles in eine richtige Struktur bringen müsste. `NoSQL` hat insbesondere den Vorteil, die Einträge in der Datenbank schnell und unkompliziert ändern zu können, falls wir für unseren Fall beispielsweise ein neues Attribut in den JSON Dateien hinzufügen wollten.
Das hat uns mit unserer agilen Arbeitsmethode gut geholfen. Auch Spring Boot bringt eine sehr gute Implementierung für die Verbindung zu MongoDB Datenbanken mit, weswegen wir uns schlussendlich für MongoDB und gegen andere NoSQL Systeme entschieden haben.

#### 3.3.3 UML-Diagramme

##### 3.3.3.1 Aufbau Rezept-JSON vom 01.06.20 (erste Version)

![Recipes Objects JSON - 01.06.2020](./img/recipes-objects-json-010620.png)

Zu Beginn des Projekts wurde sich der Aufbau eines Rezeptes überlegt.
Dadurch konnte sichergestellt werden, dass das Backend sowie das Frontend mit den gleichen Strukturen arbeiten können.
Ein `Recipe` hat dabei einige Eigenschaften als Attribute. Die Zutaten werden als ein Array von `Ingredient`s im `Recipe` dargestellt.
Die Zutaten enthalten dann jeweils Eigenschaften um die Menge und den Namen abzuspeichern.
Die Einheit wird hierbei genauer definiert und es dürfen nur bestimmte Werte hierfür verwendet werden.
Die Kochanleitung wird als Array von `Instruction`s gespeichert, die jeweils nur den entsprechenden Text enthalten.
Zunächst kann dadurch die Übersichtlichkeit der Anleitung gesteigert werden, da sie in mehrere Schritte unterteilt wird - daher wird kein einfacher String als Anleitung verwendet.
Zusätzlich kann durch das Verschachteln in einzelne Objekte ein einzelner Kochschritt in Zukunft leichter erweitert werden.
So kann zu einer `Instruction` auch ein Bild oder ein Tipp ohne weitere Komplikationen hinzugefügt werden.
Daher wird hier auch nicht ein String-Array zur Darstellung von Schritten verwendet.

##### 3.3.3.2 Aufbau Rezept-JSON vom 06.07.20 (aktuelle Version)

![Recipes Objects JSON - 06.07.2020](./img/recipes-objects-json-060720.png)

Insgesamt hat sich am Aufbau der Rezepte wenig geändert.
Es wurde zum einen eine Kurzbeschreibung als `description` hinzugefügt, um die Rezeptliste etwas mehr füllen zu können.
Sowie zum anderen ein Feld `image`, dass einen Hexstring des Bildes enthält.

##### 3.3.3.3 Rezept Objekte in Android vom 01.06.20 (erste Version)

![Recipes Objects Android - 01.06.2020](./img/recipes-objects-android-010620.png)

Die Objekte für die Rezepte entsprechen der Vorlage aus den JSON-Dateien.
Diese wurden zuerst erstellt, damit die Oberfläche sowie die Datenbankanbindung unabhängig arbeiten können und am Ende die gleichen Objekte überreichen können.
Die Objekte, die ein Rezept repräsentieren (`Recipe`, `Ingredient` und `Instruction`) implementieren hier von `Serializable`, um mithilfe der `GSON` API die JSON-Dateien in Objekte umwandeln zu können.
Zusätzlich implementieren sie von `Comparable`, um Listen von ihnen Alphabetisch ordnen zu können.
Letzteres wird vor allem bei Vergleichen in Tests verwendet.
Für die Einschränkungen für die Zutatenmenge wird die Enumeration `IngredientUnit` verwendet.
Sie enthält alle möglichen Werte, die das Attribut in der JSON annehmen kann.

##### 3.3.3.4 Android Schnittstelle Oberfläche und Datenbankanbindung vom 01.06.20 (erste Version)

![RepositoryInterface - 01.06.2020](./img/repository-interface-010620.png)

Die Schnittstelle zwischen der Oberfläche und der Anbindung wurde zu Beginn im `RepositoryInterface` festgelegt.
Dafür wurde eine Methode zum Empfangen aller Rezepte, sowie eine Methode zum Empfangen eines einzelnen Rezeptes definiert.
Die Rückgabewerte dieser Methoden wurden in einem `LiveData` Objekt aus dem Android Framework verpackt.
Durch dieses kann das Empfangen asynchron gestalten werden.
Die Oberfläche wird, nachdem sie das Objekt abonniert hat, benachrichtigt, sobald sich die inneren Objekte von `LiveData` ändern.
Die Anbindung zur Datenbank kann die inneren Objekte einfach austauschen und damit eine Benachrichtigung senden.
Somit kann sichergestellt werden, dass die Oberfläche nicht einfriert, während auf eine Antwort des Servers gewartet wird.

Die Klasse `LetsCookApp` erbt von `Application` aus dem Android Framework.
Dadurch existiert eine Instanz dieser Klasse durchgehend, während die App in Betrieb ist.
In dieser wird eine Singleton Variable von `RepositoryInterface` abgelegt.
Somit kann die Oberfläche aus ihren Activities auf die Schnittstelle zugreifen und es existiert auch immer nur eine Instanz von ihr.
Das Team der Datenbankanbindung muss nur ein Objekt von `RepositoryInterface` in der Klasse `LetsCookApp` erstellen.
Dies geschieht in der Methode `createRepository()`.

Die Implementierung des `RepositoryInterface` findet mit der Klasse `OnlineRepository` statt.
In ihr finden dann die Requests mithilfe der `Volley` API statt.
Die Oberfläche selbst bekommt von der `OnlineRepository` Implementierung am Ende nichts mit.
Theoretisch kann dieses Konstrukt später auch genutzt werden, um einen lokalen Cache von Daten abzurufen, während auf eine Antwort gewartet, oder der Server nicht erreichbar ist.
Dazu muss lediglich eine andere Implementierung eines `RepositoryInterface` genutzt werden.

Für die `Volley` API wird eine Singleton Variable einer `RequestQueue` aus der `Volley`API benötigt.
Diese wird über die Klasse `RequestQueueHandler` bereitgestellt.
Die Methode `addToRequestQueue(x)` ist hierbei nur ein direkterer Aufruf auf die `RequestQueue`, da dies oft verwendet wird.

Nach den ersten Tests mussten wir feststellen, dass `Volley` String-Requests nach ISO-8859-1 kodiert.
Unsere JSON-Dateien sind jedoch in UTF-8 gespeichert, wodurch Umlaute falsch dargestellt wurden.
Um nun die Daten doch in UTF-8 zu erhalten wird die Klasse `UTF8StringRequest` benötigt.
Diese erbt von `Request` aus der `Volley` API und verwendet in der überschriebenen Methode `parseNetworkResponse(x)` die UTF-8 Kodierung.

##### 3.3.3.5 Android Schnittstelle Oberfläche und Datenbankanbindung vom 06.07.20 (aktuelle Version)

![RepositoryInterface - 06.07.2020](./img/repository-interface-060720.png)

Dieses UML-Diagramm zeigt die Android Schnittstelle zum Ende des Projekts.
Die Klassen `LetsCookApp`, `UTF8StringRequest` sowie `RequestQueueHandler` wurden hier zur übersichtlichkeit weggelassen.
Diese sind weiterhin wie im vorherigen Kapitel beschrieben aktuell.

Die Erweiterungen gehen von den neuen Methoden im `RepositoryInterface` aus.
Diese wurden hinzugefügt, um weitere Rest-Endpoints anzusprechen.

Angefangen hat dies mit dem Abfragen von Bildern.
Dazu kann die Methode `fetchSingleImage(id)` aus dem Interface genutzt werden.
Dabei wird die Klasse `BitmapConverter` verwendet, um einen String von Hex-Zeichen in ein Bitmap zu konvertieren.
Ruft man die Methode `getAllRecipes()` auf, werden die Bilder einzeln nachgeladen. Um ein nachladen zu verhindern, z.B. wenn die Bilder bereits schonmal geladen wurden, kann die Methode `getAllRecipesWithoutUpdating()` verwendet werden.

Um nun Endpoints anzusprechen, bei denen ein Rezept an das Backend verschickt wird, wurde eine abstrakte Klasse `RecipeRequest` implementiert, die sich von `Request` aus der `Volley` API ableitet.
In dieser abstrakten Klasse wird ein `Recipe` Objekt in ein JSON-Objekt umgewandelt und in den Body der Request gesetzt.
Um bei der Umwandlung auch Bilder verwenden zu können, wird eine Platzhalterklasse `GsonRecipe` verwendet.
Mithilfe der Utils Klasse `RecipeConverter` wird ein gewöhnliches `Recipe` in ein `GsonRecipe` umgewandelt.
Dabei wird das Bild zurück in einen String aus Hex-Zeichen mithilfe von `BitmapConverter` konvertiert.
Das `GsonRecipe` kann dann in ein JSON-Objekt verwandelt werden, welches alle Daten für das Backend enthält.

Die Klassen `PostRecipeRequest` sowie `PutRecipeRequest` vererben anschließend von der abstrakten Klasse `RecipeRequest` dadurch können diese beiden Funktionen zum Ansprechen der restlichen Backend Endpoints leicht implementiert werden.
Am Ende geben beide Klassen einen Enum von `RecipeResponse` zurück, auf welchen die Oberfläche reagieren kann.
In diesem sind Werte für Fehler sowie Erfolge möglich, um reaktionen zu vereinfachen.

##### 3.3.3.6 Sequenzdiagramm - Erstellen eines Rezeptes durch den Nutzer

![Sequenzdiagramm erfolgreicher POST](./img/sequenzdiagramm-post-success.png)

Dieses Sequenzdiagramm bildet den zeitlichen Ablauf einer erfolgreichen Rezepterstellung ab.
Die Akteure / Klassen des App-Frontends sind blau hervorgehoben, das Backend grün.
Der Ablauf wird auf Klassenebene dargestellt, allerdings wird nicht dargestellt welche Methoden Schritt für Schritt durchlaufen werden.

![Sequenzdiagramm fehlerhafter POST](./img/sequenzdiagramm-post-error.png)

Dieses Sequenzdiagramm bildet den zeitlichen Ablauf einer nicht erfolgreichen Rezept Erstellung ab,
da die Validierung des geposteten JSON-Bodys in der REST-Anfrage fehlerhafte Werte enthält.
Somit schlägt die Validierung des DTOs im Controller-Layer fehl und eine Exception wird geworfen.

##### 3.3.3.7 Use Case vom 01.06.20 (erste Version)

![Use Case](./img/UseCase.jpg)
Dieses Use Case Diagramm beschreibt verschiedene Szenarien, die in Let’s Cook eintreten können. Neue Benutzer können sich Registrieren und Rezepte ansehen, aber nur registrierte Benutzer dürfen sich einloggen, neue Rezepte veröffentlichen und eigene veröffentliche Rezepte bearbeiten und löschen.
Es werden die Eingaben überprüft beim Erstellen von neuem Rezept sowie beim Bearbeiten bereits veröffentlichten Rezept. Beim Löschen und Bearbeiten des Rezepts wird zusätzlich Nutzer ID über-prüft, damit es nicht möglich wäre fremde Rezepte zu löschen und zu bearbeiten.  

##### 3.3.3.8 Use Case vom 10.07.20 (aktuelle Version)

![Use Case](./img/UseCaseUpdate.jpg)
Da die Benutzerverwaltung nicht implementiert wurde, hat sich in Use Case Diagramm einiges geändert.  Jetzt darf jeder Benutzer von Let’s Cook App alle Rezepte ansehen, bearbeiten, löschen sowie neue Rezepte erstellen.

#### 3.3.4 Entwurf der Benutzeroberfläche

Aufgrund der Anforderungen sollen Benutzeroberflächen entworfen werden.
Die App Let's Cook soll einen einheitlichen Grundaufbau haben.
Der Header, der aus App-Name und einem Off Canvas Menü bestehen wird, soll bei allen Layouts vorhanden sein.

Beim Öffnen der App soll zuerst eine Startseite eingeblendet werden, bevor es zum Login Layout wechseln soll.
Hier muss der Benutzer eine Möglichkeit bekommen, ohne Login die App zu starten, sich zu registrieren oder sich einzuloggen.
Anschließend soll es zum Hauptlayout mit den Rezeptlisten wechseln.
Die Rezeptobjekte, dieser Liste werden aus der Datenbank geladen und beinhalten ein Rezeptbild, Rezeptname und eine kurze Beschreibung.

![Mockup Rezeptliste](./img/Rezeptliste.jpg)

Alle Rezeptobjekte in der Liste müssen anklickbar sein, damit die Rezeptdetailansicht geöffnet werden kann.
Diese soll aus einem Rezeptnamen, Bild, Zutatenliste, eine Angabe für wie viele Personen das Rezept ausgelegt ist und eine Schrittanleitung bestehen.
Bei diesem Layout sollen Buttons Bearbeiten und Löschen vorhanden sein, die oben in dem Header positioniert werden.
Durch Betätigen des Bearbeiten Buttons kommt man zur Bearbeitungsansicht.
Diese soll Inputfelder für Rezeptname, Kurzbeschreibung, Zutaten und Zubereitungsschritte sowie Buttons für Bildänderung, Verwerfen und Rezept speichern haben.

![Mockup Rezeptlayout](./img/Rezept.jpg)

Das Off Canvas Menü muss ein Logo, App-Name, Home und ein *Rezept erstellen* Button beinhalten, damit schnell zu einem gewünschten Layout gewechselt werden kann.
Durch Betätigen des *Home* Buttons im Off Canvas Menü kommt man zur Rezeptlistenansicht und durch Betätigen des *Rezept Erstellen* Buttons zum Rezeptvorlage.
Dieses Layout soll Inputfelder für Rezeptname, Kurzbeschreibung, Zutaten und Zubereitungsschritte, sowie Buttons für Bildupload, *Abbrechen* und *Speichern* haben.

### 3.4 Implementierung

#### 3.4.1 Pipelines

##### 3.4.1.1 Frontend-Pipeline

Bei der Entwicklung der Pipeline für die Android-App hielten wir uns an [dieses Tutorial](https://android.jlelse.eu/android-gitlab-ci-cd-sign-deploy-3ad66a8f24bf).  

Die Pipeline ist  in das CI|CD-Pipeline-System von GitLab integriert und damit sehr vielseitig nutzbar.
Die Pipeline-Durchläufe mit den einzelnen Jobs und deren Ergebnisse sind direkt an der Oberfläche von GitLab sichtbar.
Die Konfiguration der Pipeline befindet sich in dem File `gitlab-ci.yml`.  

Die Pipeline vollzieht insgesamt fünf Jobs in vier verschiedenen Phasen:

| Build | Test  | Release  |  Deploy |
|---|---|---|---|
| lintDebug </br> assembleDebug  | testDebug  | assembleRelease  | deployRelease  |

In der Build-Phase werden zwei Jobs parallel gestartet.
Der Lint-Check (lintDebug) prüft die Code-Syntax auf Ungereimtheiten.
Schwerwiegende Verstöße gegen Conventions oder gar Fehler in der Syntax führen hier zum Abbruch der Pipeline.
Parallel läuft ein Bauen des Projekts. Hier wird die App einfach (wie auch in der IDE) gebaut und geprüft, ob dabei Fehler auftreten.
Auch hier bricht die Pipeline bei Problemen ab.  

Hat alles funktioniert, startet die Test-Stage.
Hier werden die Tests des Projekts ausgeführt (testDebug).
Dieser Job berücksichtigt vor allem die Unit-Tests.
Schlägt hier eine Instanz fehl, bricht die Pipeline ebenfalls ab.  

Haben die beiden vorherigen Stages keine Fehler gemeldet, startet nun die Release-Stage.
Die App wird nun als .apk-Datei gebaut und anschließend mit einem fest in Git hinterlegten Key signiert (assembleRelease).
Die Generierung des Keys erfolgte einmalig im Android Studio.
Der Alias-Name des Keys, das KeyStore-File in Base64, sowie das Passwort des Keys und des KeyStores wurden in den CI|CD-Variablen in Git hinterlegt und werden jedes Mal für die Signierung genutzt.  

In der letzten Stage (Deploy-Stage) wird die App nun ausgeliefert.
Dafür wird die .apk-Datei durch den deployRelease-Job in eine eigens eingerichtete Dropbox platziert.
Der dafür verwendete Autorisierungstoken ist ebenfalls in den oben genannten Variablen gespeichert.
Abschließend setzt die Pipeline einen HTTP-POST an eine Schnittstelle eines Drittanbieters ab.
Darin ist eine in den Variablen angegebene Liste an E-Mail-Adressen und ein Betreff, sowie ein Bo-dy für eine E-Mail enthalten. Letztere sind im Projekt selbst über Textdateien festlegbar.
Dabei stehen über die Pipeline bestimmte Variablen, wie ein selbst erstelltes Changelog, der Name der App und die Download-Url für die apk zur Verfügung.
Diese Platzhalter im Mail-Template werden von der Pipeline mit den passenden Daten ersetzt.
Der Drittanbieter nimmt den POST entgegen und sendet die entsprechend konfigurierte E-Mail.  

Durch die Mail werden die Entwickler über die automatische Erzeugung einer neuen apk informiert und können sich die App herunterladen oder den Link mit potenziellen Kunden teilen.
So gelangen auch diese an die App. Das Installieren auf dem Smartphone funktioniert durch einen einfachen Klick auf die über den Link geladene Datei.

Die Pipeline läuft bei jedem Merge auf einen Branch in Git los.
Dadurch ist gewährleistet, dass nur baubare, syntaktisch korrekte und die Tests erfüllende Versionsstände auf die Hauptzweige eingebracht werden können.
Das verhindert eine nachträgliche Fehlersuche und zeigt dem Entwickler eventuell übersehene Fehler im aktuellen Branch.  

Zudem läuft die Pipeline bei Änderungen am Master-Branch an.
Da dieser Branch unsere stabile App-Version darstellt, ist ein Bauen der jeweils neuen Version vor allem für die Nutzer interessant.  

Bei allen anderen Änderungen an jeglichen Branches wird zwar eine Pipeline angelegt, aber direkt gestoppt.
Auf Wunsch kann diese manuell angestoßen werden. Auch ein manueller Start der Pipeline ist über GitLab möglich.

##### 3.4.1.2 Backend-Pipeline

Ähnlich wie Frontend-Pipeline, ist auch die Backend-Pipeline in das CI|CD-Pipeline-System von GitLab integriert.
Alle Durchläufe der Pipeline, mit den einzelnen Jobs und deren Ergebnisse, sind in GitLab sichtbar.
Die Pipeline wird in der Datei `gitlab-ci.yml` konfiguriert und aufgebaut.  

Die Backend-Pipeline hat insgesamt drei Stages:

1. build
2. test
3. deploy

Diese drei Stages werden in der oben aufgeführten Reihenfolge durchlaufen.
Tritt bei einer Stage ein Fehler auf, wird die Pipeline und somit das Durchlaufen der weiteren Stages abgebrochen.

In der ersten Stage `build` wird das Projekt kompiliert und das lauffähige jar-File gebaut.
Dabei wird ein Maven `package` Befehl ausgeführt.
Diese Stage dient zur Überprüfung, ob keine Kompilierfehler durch die neuesten Änderungen aufgetreten sind und ein problemloser jar-Bau möglich ist.

In der zweiten Stage `test` wird das Projekt kompiliert und alle Unit Tests werden automatisch ausgeführt.
Es wird ein Maven `test` Befehl abgesetzt.
Diese Stage dient zur Überprüfung, ob alle Unit Tests *grün* sind, d.h. das die neuesten Änderungen keine alten Funktionen stören.
Ist ein Unit Test nicht erfolgreich gelaufen, gilt diese Stage als gescheitert und die Pipeline bricht ab.

In der dritten Stage `deploy` wird das Projekt kompiliert und das fertige jar-File auf `cloud foundry` hochgeladen.
Es wird ein Login in die Cloud Foundry Umgebung ausgeführt und anschließend ein Cloud Foundry `push` Befehl abgesetzt.
Die Credentials für die Cloud Foundry Umgebung sind in GitLab Environment Variablen hinterlegt.  
Diese Stage dient dazu, unsere Rest-API im Internet für das App-Frontend zugänglich zu machen.

Die ersten beiden Stages werden bei jedem `git push` auf allen *branches* angestoßen und durchlaufen.
Deployed wird allerdings nur, wenn die Stages auf den *master branch* mit dem push Befehl hochgeladen wurden.

[Wir sind nach folgendem Tutorial vorgegangen.](https://docs.gitlab.com/ee/ci/examples/deploy_spring_boot_to_cloud_foundry/)

#### 3.4.2 Aufbau der Backend Architektur

Die Java Spring Boot REST-Api wurde mit einer Drei-Schichten-Architektur implementiert. Diese besteht aus dem Controller, Service und Repository Layer

##### Controller-Layer

In der Controller Schicht befinden sich alle Klassen, welche für die REST-Operationen verwendet werden. Mit ihr wird daher von außen interagiert.
Im Interface `RecipeController` werden alle REST-Endpunkte, mit den dazugehörigen Informationen, definiert.
Diese Endpunkte werden letztendlich in der Klasse `RecipeControllerImpl` implementiert.
Diese hat eine Abhängigkeit zur Service-Schicht und dem externen ModelMapper.
Eine weitere Aufgabe der Controller Schicht ist die Kapselung der DTO und Entity Objekte.
Dies wird durch generisches Mapping mit dem ModelMapper in zwei privaten Methoden in der `RecipeControllerImpl` Klasse erreicht.
Somit werden die öffentliche DTOs nur in der obersten Schicht verwendet.
Die Klasse `GlobalRestExceptionHandler` gehört auch zur Controller Schicht und erbt von der Spring Klasse `ResponseEntityExceptionHandler`.
Letztendlich dient sie dazu, zu regeln, wie die API bei auftretenden Exceptions reagiert.

##### Service-Layer

In der Service Schicht sind ein Interface (`RecipeService`) und eine Singleton Klasse (`RecipeServiceImpl`) enthalten.
Die Service Klasse hat eine Abhängigkeit zur Repository Schicht.
Diese wird benötigt, da sie Datenbankzugriffe an den Repository-Layer weiterleitet und diese ggf. validiert und Fehler wirft.

##### Repository-Layer

Die Repository Schicht implementiert die MongoDB Datenbankabfragen.
Alle Datenbank Operationen sind im `RecipeRepository` Interface, welche von der Spring Klasse `MongoRepository` erbt, gekapselt.
Aufgrund der genutzten Spring-Data, muss dieses nicht gesondert von uns selbst implementiert werden.

#### 3.4.3 Implementierung der API-Anbindung

Die API-Anbindung wurde (wie in den UML-Diagrammen ersichtlich) unter ein Interface gelegt.
Dieses Interface `RepositoryInterface` und deren Implementierung wurde laufend von einem Teilteam entwickelt.
Dadurch war es möglich, dass die Entwickler der Oberfläche eine leere Interface-Methode ansprechen konnten, während der eigentliche Code für die Requests noch in der Arbeit war.  

Die Implementierung selbst findet in der Klasse `OnlineRepository` statt.
Hier sind die jeweiligen Methoden zum Schreiben, Lesen und Löschen implementiert.
Für die HTTP-Requests entschieden wir uns für das `Volley`-Framework.
Dies ist der Standard in der Android-Dokumentation und war deshalb für uns die erste Wahl.
Das Framework bietet eine `RequestQueue` an, in welche man Request-Objekte zur Ausführung stellen kann.
Diese Request-Objekte können von verschiedenen Typen sein, welche alle von der Elternklasse `Request` des Frameworks abgeleitet sind.
Über den Methodenaufruf `start()` der `RequestQueue` werden alle in der Queue stehenden Requests ausgeführt und die jeweiligen Responses an die Listener weitergegeben.
Die Typen der Responses hängen dabei von den Request-Typen ab.
Je nach Erfolg oder Scheitern der Request wird nach der Antwort eine Fehlerantwort oder eine Erfolgsantwort behandelt.  

Die RequestQueue wird über die Klasse `RequestQueueHandler` verwaltet, welche das `Singleton`-Prinzip anwendet um eine einzige und eindeutige Instanz zu verwalten und keine mehrfachen Queues zu ermöglichen.  

Bei unseren GET-Abfragen nutzen wir eine selbst erstellte Klasse `UTF8StringRequest`.
Diese ist von einer String-Request abgeleitet und erweitert die Grundfunktionalität durch ein UTF-8-Coding.
Dies ist nötig, da wir in unseren Rezepten auch Umlaute haben, welche sonst nicht richtig verarbeitet werden.
Dazu wurden die Methoden zur Übermittlung der Antwort entsprechend angepasst.
Der Request wird neben der URL und dem Http-Verb GET auch ein Listener für den Erfolgs- und einer für den Fehlerfall übergeben.
Im Fehlerfall wird `null` bei einem Einzelrezept-Abruf bzw. eine leere Liste bei einem Listenrezept-Abruf zurückgegeben.
Bei einer erfolgreichen Request wird das Einzelrezept bzw. die Liste der Rezepte entsprechend serialisiert.
Dazu wird die Library `GSON` genutzt, welche als Typ unser eigens erstelltes `Recipe`-Model (bzw. eine Liste davon) erhält.
Die Models liegen im package `models` und sind in mehrere Klassen aufgeteilt.
Nach dem erfolgreichen Abruf der Rezeptdaten wird für jedes Rezept noch ein separater GET für das nachladen des Bildes ausgeführt.
Dieses wird dann in die Response dynamisch nachgeladen (genaueres dazu weiter unten).
Durch dieses Vorgehen ist es möglich, die Bilder, welche die längste Ladezeit nenötigen, erst nach Abschluss des ersten Datenerhalts nachzuladen und so die Rohdaten ohne Bilder auf der Oberfläche anzeigen zu können.
Die Oberfläche friert so nicht ein und auch bei einer langen Liste an Rezepten ist eine schnellere Ladezeit möglich.  

Über eine GET-Abfrage der gleichen Struktur kann auch nur ein einzelnes Bild abgefragt werden, ohne die restlichen Rezeptdaten erneut zu erfragen.

Die Rückgabewerte werden über `LiveData`-Objekte ausgegeben.
Diese werden vor Erhalt der Antwort des Servers zurückgegeben.
Die Oberfläche kann sich auf das Objekt "abonnieren" und wird so bei einer Veränderung des Inhalts benachrichtigt.
Auch das verhindert ein einfrieren der UI und es muss nicht auf die Request synchron gewartet werden.
Das Nachladen der Bilder ist durch dieses Prinzip einfach abzudecken.  

Bei den POST-Requests nutzen wir eine selbst erstellte Klasse namens `PostRecipeRequest`, welche sich von der abstrakten Klasse `RecipeRequest` ableitet.
Auch diese ist eine Kindklasse der `Request` aus dem Volley-Framework.
Dabei wird das Verarbeiten des Request-Bodys und das Listener-Handling selbst implementiert.
Der Body kann bei der Initialisierung der Request als Recipe-Objekt mitgegeben werden.
Die Verarbeitung findet dann in einer eigenen Methode statt.
Bei Erfolg wird der Erfolgslistener bedient.
Bei einem Fehler wird die Art des Fehlers gefunden und anschließend an den normalen Listener weitergegeben.
Dadurch erhalten wir immer eine Antwort unseres Rückgabetyps (siehe nächste Absatz) und müssen keinen seperaten Error-Listener implementieren.  

Die Antwort der POST-Requests ist ein Enum des Typs `RecipeResponse`.
Dieses Enum kann mehrere Werte annehmen.
Darunter `SUCCESS` für eine erfolgreiche Abfrage, `ERROR` bei einem unbekannten Fehler, `TIMEOUT` bei einem Timeout-Error oder jeweils eigene Werte für Fehler im Request-Body der Oberfläche (z.B. fehlende oder ungültige Felder).
Durch die Antwort eines Enums, kann die Oberfläche entsprechende Fehlermeldungen für jeden Fehlertyp anzeigen und den Nutzer zur Verbesserung seines Rezeptes aufrufen.

Die DELETE-Request funktioniert wie die GET-Abfrage mit einer `UTF8StringRequest`.
Dabei wird ein Delete, welcher definitionsgemäß ohne einen Request-Body funktioniert, mit der Id des zu löschenden Objekts in der URL gesendet.
Je nach Erfolg oder Fehlschlag der Request wird ein Boolean `true` oder `false` zurück gegeben.

Um das Editieren der Rezepte zu ermöglichen wurde eine PUT-Request implementiert. Diese funktioniert exakt wie die POST-Requests, nur dass es eine neue Klasse `PutRecipeRequest` genutzt wird. Diese leitet sich, wie auch die `PostRecipeRequest`, von der abstrakten Klasse `RecipeRequest` ab und unterscheidet sich einzig und allein im genutzen Http-Verb, welches hier logischerweise `PUT` statt `POST` ist. Der Rückgabetyp und das Listener- und Error-Handling bleiben gleich.

### 3.4.4 Implementierung der Benutzeroberfläche

Benutzeroberflächen wurden Mithilfe des Android Studios auf der Grundlage, die im 3.3.4 Entwurf der Benutzeroberfläche beschrieben wurde, entworfen.
Leider konnte das Login Layout aus zeitlichen Gründen nicht fertiggestellt und implementiert werden.

Benutzeroberflächen bestehen aus zwei Bestandteilen: Activity und View.
Die grafische Oberfläche, wie Buttons, Texte, Bilder etc. wird in der Viewdatei (xml) definiert.
Welche Aktionen hinter einem Button hinterlegt sind, werden in der Activity mit Java-Code festgelegt.

Zuerst wurde das Off Canvas Menü `activity_main_drawer.xml` erstellt, das zwei Buttons *Home* und *Rezept erstellen* enthält.
In der `nav_header.xml` wurde der Header des Off Canvas Menü Header, mit imageView für das Logo und zwei TextView für den App-Namen und die Kurzbeschreibung, entworfen.
Dieses Menü steht dem Benutzer immer zur Verfügung, egal in welchem Layout er sich gerade befindet und bietet eine Möglichkeit zwischen den Layouts zu wechseln.
Dafür wurde die `onNavigationItemSelected()` Methode implementiert, die eine bestimmte Activity startet, je nachdem welcher Button gedrückt wurde.

![Off Canvas Menü](./img/Off-Canvas-Menü.jpg)

Danach wurde die Hauptview `home_activity.xml` mit der Rezeptliste erstellt.
In `MainActivity.java` wird diese Liste dann bestückt. Die Klasse `ArrayAdapterAllRecipes.java` bindet jedes Rezept mit einer eigenen View an die Liste. Die View der Rezepte wird in `list_layout_all_recipes.xml` definiert. 
Das Grundgerüst für ein Rezeptobjekt besteht aus einem ImageView für ein Rezeptbild und zwei TextView Felder für Rezeptname und Kurzbeschreibung des Rezepts.
Zusätzlich wurde in dem Hauptview ein Startfenster eingefügt, welches beim Start der App einmal für kurze Zeit angezeigt wird.
Dieses besteht aus einem ImageView für das Logo und einem TextView für den Namen der App.

![Rezeptliste](./img/Rezeptliste_app.jpg)

Als Nächstes wurde die Rezeptdetailansicht `recipe_activity.xml` implementiert.
Dieser View besteht aus einem ImageView für Rezeptbild und mehreren TextView für sämtliche Daten, die in Anforderungen vorgegeben wurden.
In der Activity `RecipeActivity.java` werden Daten aus einem Rezept Objekt verarbeitet und in die entsprechenden Felder übergeben.
Für diesen View wurden zwei zusätzliche Buttons *Löschen* und *Bearbeiten* in `recipe.xml` erstellt und mit `onCreateOptionsMenu()` Methode im Header positioniert.
Die Methode `onOpionsItemSelected()` wird in dem Fall aufgerufen, wenn einer der Buttons betätigt wird.

Anschließend wurde der View `insert_recipe_activity.xml` für das Erstellen eines neuen Rezepts erstellt.
Dieser View hat 4 TextInputLayout Felder für Rezeptname, Kurzbeschreibung, Personenanzahl und Zubereitung, eine Liste für Zutaten sowie 4 Buttons für Bildupload, Zutaten hinzufügen, abbrechen und speichern des Rezepts.

Die Zutaten werden Mithilfe von Pop-Up Fenster hinzugefügt.
Dafür wurde ein Pop-Up View `select_ingredient.xml`, die einen Spinner Element, zwei EditText Felder und zwei Buttons *Abrechen* und *Hinzufügen* hat und `ArrayAdapterIngredients.java` mit `ArrayList<>` für die Zutaten anzeige.
In `InsertRecipeActivity.java` wird dieses Pop-Up Fenster mit  der `selectIngredient()` Methode geöffnet.
In dieser Methode werden die Eingaben beim Drücken auf Button *Hinzufügen* überprüft.
Falls die Felder leer sind wird entsprechend ein Hinweis angezeigt „Überprüfe die Eingaben“.  

![Rezept erstellen](./img/Rezept-erstellen1.jpg)

![Rezept erstellen](./img/Rezept-erstellen2.jpg)

Um Rezepte zu speichern wurde die Methode `save()` implementiert.
Hier werden die TextInputLayout Felder überprüft, ob diese ausgefüllt sind und falls es nicht der Fall ist, werden die leeren Felder rot und mit einem Vermerk markiert, da es nicht leer sein darf.

Zuletzt wurde die Funktionalität für den Button Bearbeiten, der in `recipe_activity.xml` in Header positioniert wurde, in `InsertRecipeActivity.java` implementiert.
Wenn der Button gedrückt wird, werden die Daten von dem Rezept mit der Methode `loadRecipe()` den in `insert_recipe_activity.xml` View für Bearbeitung geladen.
Anschließend werden die Rezeptdaten mit der Methode `save()` überschrieben.

### 3.5 Soll-Ist-Analyse

#### 3.5.1 Meilenstein „Kick-Off“

Als die Gruppe gebildet war, haben wir bei einem Online Meeting die Anforderungen, die das Projekt erfüllen soll, besprochen. Bis zu dem Meilenstein „Kick-Off“, das am 07.05 stattgefunden hat, wurden 3 Anforderungen ausgearbeitet. Wir haben uns vorgenommen eine Definition of Done, Scrum Board und ein Grundgerüst für die Dokumentation zu erstellen.

Alle 3 Anforderungen wurden innerhalb des festgesetzten Zeitraums erfolgreich abgeschlossen und am 06.05 von allen Teammitglieder abgenommen. Anschließend haben wir eine grobe Aufgabenplanung im Scrum Board bis zu dem Meilenstein „Alpha“ erstellt.

#### 3.5.2 Meilenstein „Alpha“

Noch vor dem Meilenstein „Kick-Off“ haben wir 5 Anforderungen grob ausgearbeitet. Bis zu dem Meilenstein „Alpha“, welcher am 10.06 stattgefunden hat, wollten wir backend sowie frontend build Pipelines erstellen, Testdaten für die Datenbank schreiben, eine Liste aller Rezepte sowie eine Rezept Detailansicht implementieren. Im Laufe des Prozesses haben wir beschlossen noch zu der Rezeptliste ein Bild sowie eine Kurzbeschreibung hinzufügen. Somit haben wir auch unsere Anforderungsliste angepasst.

Backend und frontend build Pipelines wurden gleich am 06.05 fertiggestellt. Ein Tag danach wurden Testdaten für die Befüllung der Datenbank erstellt und die Anforderung somit abgeschlossen. An der Liste aller Rezepte und Rezept Detailansicht wurde 3 Wochen gearbeitet und am 03.06 von dem gesamten Team abgenommen.

Anschließend haben wir wieder eine grobe Aufgabenplanung im Scrum Board bis zu dem nächsten Meilenstein „Abschluss“ gemacht.

#### 3.5.3 Meilenstein „Abschluss“

In der letzten Phase haben wir uns 7 Anforderungen bis zu dem Meilenstein „Abschluss“ vorgenommen. Diese waren unter anderem die Benutzerregistrierung sowie Grundfunktionalitäten, wie neues Rezept erstellen, zu bearbeiten und zu löschen. Dazu eigene Rezepte bearbeiten und löschen und eine Startseite. Gleich bei der „Alpha“ Besprechung haben wir erwähnt, dass wir die Benutzerverwaltung ganz am Schluss machen werden, falls noch Zeit übrigbleibt. Uns war es wichtiger die Grundfunktionalitäten zu implementieren, damit die App ihre Grundidee erfüllt.

Am 06.07 wurden die zwei Aufgaben „neues Rezept erstellen“ und „Rezept löschen“ mit Erfolg abgeschlossen. Ein Tag vor dem Meilenstein „Abschluss“, das am 17.07 stattgefunden hat, wurden dann die Anforderungen „Rezept Bearbeiten“ sowie „Startseite“ von dem Team akzeptiert.  

Benutzerverwaltung wurde aus zeitlichen Gründen nicht implementiert. Somit haben wir die Anforderungen „Benutzerregistrierung“ sowie „Eigene Rezepte löschen und bearbeiten“ nicht erfüllt.

## 4 Dokumentation

### 4.1 Testdokumentation

### 4.1.1 API-Anbindung

Die Tests für die Api-Anbindung wurden mithilfe von `Mockito` und `Powermockito` implementiert.
Dazu wurden die sogenannten `Mocks`, also Attrappen von bestimmten Klassen, verschachtelt ausgegeben.
Dadurch ist es möglich die Tests unabhängig vom Verhalten des Servers durchzuführen und allein die Verarbeitung der Daten in der App zu testen.  

Das Ausführen der Requests erfolgt normalerweise über eine `RequestQueue`, welche man über die Klasse `RequestQueueHandler` erhält.
In diese Queue stellt man nun die gewünschten Requests mit den entsprechenden Parametern wie HTTP-Verb, URL, Listener und gegebenenfalls Request-Body.
Wenn man die Queue startet, werden alle enthaltenen Requests als Http-Calls ausgeführt und die jeweiligen Listener mit den Daten bedient.  

In unseren Tests erhält die App eine `RequestQueueHandler`-Attrappe, welche eine `RequestQueue`-Attrappe ausliefert.
Beim Erstellen einer neuen Request wird auch nur eine Attrappe der jeweiligen Klasse erstellt.
Die übergebenen Parameter bei der Erstellung (darunter die oben genannten Daten wie URL usw.) werden zusammen mit der Request-Attrappe in einer Map gespeichert.
So ist eine Rückführung von der Attrappe zu den eigentlichen Parametern möglich.  

Soll jetzt eine solche Request-Attrappe in den `RequestQueue`-Mock eingefügt werden wird diese einfach in einer Liste für die spätere Ausführung gespeichert.
Sobald der Mock-Queue der Startbefehl erteilt wird, werden alle Listener der Requests in der Liste mit einer voreingestellten Antwort aufgerufen, so als hätte der Server seine Antwort gesendet.
Die abgearbeiteten Requests werden abschließend aus der Queue entfernt und die abgearbeitete URL mit dem Http-Verb (und bei POSTs/PUTs mit dem Request-Body) werden jeweils separat gespeichert.  

Wir haben für alle 4 Request-Typen (GET, POST, PUT und DELETE) jeweils eine eigene Testklasse erstellt, welche im `setup()` die jeweiligen Konfigurationen für die Mocks angibt. Die Mocks unterscheiden sich hier beispielsweise im Typ der Request. Bei DELETE und GET-Abrufen nutzen wir die `UTF8StringRequest`, wohingegen wir bei POST und PUT-Requests eine jeweils eine eigene Request-Klasse instanziieren.  

Die Hintergrundlogik der Tests liegt dabei in einer eigenen Klasse `OnlineRepositoryTestHelper`.
Diese Klasse wird in den 4 Testklassen entsprechend verwendet und konfiguriert.
Damit eine Redundanz der Hilfsklasse vermieden wird, kann die Klasse mit allen genutzten Request-Typen umgehen.
Dazu nutzt sie oft nur die gemeinsame Elternklasse `Request` und unterscheidet nur bei speziellen Anweisungen, wie dem Bedienen der Listener mit verschiedenen Datentypen oder dem Abspeichern der Parameter, zwischen den speziellen Request-Arten.
Zur Abspeicherung der Parameterdaten wird eine eigene abstrakte Klasse `HelpRequest` für die benötigten Vorgehensweisen genauer spezifiziert, um auch hier eine einheitliche Code-Basis zu gewährleisten.

Die Antworten für die Listener (Json-Strings bei GET und DELETE; RecipeResponses bei POST und PUT) sind im jeweiligen Unit-Test selbst festzulegen. Dabei wird eine Liste der Antworten definiert, welche nacheinander vom simulierten Server ausgegeben wird.
So lässt sich beispielsweise auch das Nachladen der Rezeptbilder testen, da hier mehrere Requests in einer Abfrage gesendet werden.  

Durch diese Form der Abstraktion mit Mocks ist es möglich, sowohl das richtige Aufrufen der API mit URL, Verb und gegebenenfalls Request-Body (durch die Liste der ausgeführten Requests), wie auch den richtigen Umgang mit den Serverdaten (durch Aufruf der Listener) zu prüfen.

Durch die Implementierung eigens abgeleiteter Klassen für die POST und PUT-Requests wurde auch das Fehler-Handling überschrieben.
Die Requests liefern bei einem Fehler einfach ein Enum für den entsprechenden Fehlertyp zurück.
Dazu wurde eine eigene Fehlererkennung implementiert, welche in den Klassen `PostRecipeRequestTest` und `PutRecipeRequestTest` abgetestet wird.
Dabei wird das Fehlerobjekt, welches eigentlich vom Backend-Server kommt, an den Error-Listener übergeben.
Geprüft wird, ob der normale Listener anschließend mit dem entsprechenden Fehler-Enum aufgerufen wird.

Zudem wurden Unit-Tests zur Konvertierung der Rezeptbilder und zum Vergleich von Rezepten geschrieben.
Erstere umfassen das Konvertieren eines HEX-Strings in ein Byte-Array und umgekehrt.

### 4.1.2 Server Backend / Rest-Api

Die Software Qualität der Rest-API im Backend wird anhand von Java Unit Tests mit dem JUnit 5 und Spring Framework gewährleistet.
Es gibt jeweils eine Testklasse pro `Controller`, `Service` und `Repository` Klasse und eine Testklasse für den SpringBoot Integrationstest.

In der Controller Testklasse wird explizit nur die Controller-Schicht getestet.
Diese greift zwar auf die Service-Schicht zu, jedoch wird diese Schichte bei diesen Tests *gemockt*.
Des Weiteren wird das *slicing* der Controller-Schicht durch die SpringBoot Annotation `@WebMvcTest` hervorgerufen.
Diese bewirkt das nur *Beans* der Controller-Schicht von Spring in diesem Testdurchlauf erstellt werden und damit auch verwendet werden können.
REST-Abfragen zum Testen werden durch das Spring eigene MockMvc abgegeben, dabei wird **kein** Tomcat-Webserver im Test gestartet, auch dieser ist im Hintergrund *gemockt*.

In der Service Testklasse wird explizit nur die Service-Schicht getestet.
Diese greift zwar auf die Repository-Schicht zu, jedoch wird auch diese Schicht für die Tests *gemockt*.

In der Repository Testklasse wird explizit nur die Repository-Schicht getestet, diese greift auf die Datenbank zu.
Allerdings greifen wir im Test nicht etwa auf unsere Produktiv-Daten zu, sondern auf eine *embedded* MongoDB.
Diese wird durch eine eigene Maven-Dependency und das Spring Framework konfiguriert und automatisch erstellt bzw. am Schluss gelöscht.
Es wird eine tatsächliche MongoDB Datenbank auf dem eigenen Rechner erstellt auf welche dann im Test connected wird.
Anschließend werden, "per Hand" Daten in den Test Methoden in die Datenbank eingefügt, damit diese später abgerufen werden können zum Testen.
Dieser Test ist ein Spring `DataMongoTest`. Der Zugriff auf die *gemockten* Daten erfolgt ganz normal über die Repository Klasse.

Bei unserem `SpringBootTest`, dem Integrationstest der Rest-API, werden **alle** Schichten der Anwendung durchlaufen und damit getestet.
Alle *Beans* der Anwendung werden in den Spring Kontext geladen.
Des Weiteren wird bei diesen Tests auch der Tomcat-Webserver gestartet und konfiguriert.
Die Testdaten werden, wie beim Testen der Repository-Schicht, in eine *embedded* MongoDB geladen.
API-Aufrufe werden mit dem Spring eigenen `TestRestTemplate` abgesetzt.

![Code Coverage Backend](./img/Code-Coverage-Backend.png)

![Code Coverage Backend Klassen](./img/Code-Coverage-Backend-Classes.png)

Auf den beiden Bildern sind die Ergebnisse der Intellij Ultimate "Run Tests with Code Coverage" Option zu sehen.
Die Screenshots wurden nach Abschluss der Implementierung des Backends erstellt.
Insgesamt wurden dafür 49 Test Cases benötigt.

### 4.2 REST-Api Dokumentation

Für die Dokumentation der REST-API wird OpenAPI v3 und Swagger-UI verwendet.
Integriert wurden diese Dokumentations-Tools, durch das Einfügen von zwei SpringDoc Dependencies in die `pom.xml` des Spring Boot Projektes.
In der `application.yaml` ist festgelegt, unter welchem Pfad / Endpunkt die Dokumentation abrufbar ist.

OpenAPI generiert aus dem vorhanden Quellcode wahlweise eine JSON oder YAML Datei, welche alle wichtigen Informationen zu den REST-Endpunkten knapp zusammenfasst.
Diese beiden Dateien unterscheiden sich nicht im Inhalt, sondern nur in der Aufbereitung der Informationen, bedingt durch das gewählte Dateiformat.

Die JSON OpenAPI-Datei ist abrufbar durch den GET Endpunkt `.../api-docs`.

Die YAML OpenAPI-Datei ist abrufbar durch den GET Endpunkt `.../api-docs.yaml`.  

Swagger-UI bereitet die Informationen von OpenAPI nun grafisch mit einer generierten HTML Seite auf.
Diese ist unter `.../swagger-ui.html` erreichbar.

![Swagger-Startseite](./img/Swagger-Startseite.png)

Auf der Swagger-UI Startseite werden alle RestController und deren Endpunkte aufbereitet.
Unten sind alle von der API verwendeten Schemas / Data-Transfer-Objects, welche entweder zurückgegeben oder angenommen werden.

![Swagger-POST-Beschreibung](./img/Swagger-POST-Beschreibung.png)

Alle Endpunkte sind klickbar, um eine genauere Beschreibung dieser zu erhalten.
Hier beim POST eines neuen Rezeptes, wird beschrieben, dass unbedingt ein RequestBody in der Abfrage enthalten sein muss.
Auch sieht man gleich, dass dieser Body ein JSON Objekt mit einem gewissen Aufbau sein muss.
Unten sind die möglichen Responses mit Status Code und eventuellem ResponseBody grafisch dargestellt.

![Swagger-GET-Try-it-out](img/Swagger-GET-Try-it-out.png)

Die Swagger-UI bietet auch die Möglichkeit Abfragen direkt auszuprobieren und somit die API zu testen.
Man sieht dann sofort welche CURL oder Request URL man angeben müsste und welche Antwort man bekommt.

![Swagger-Rezept-DTO](./img/Swagger-Rezept-DTO.png)

Alle unten aufgeführten Schemas sind auch klickbar und werden genau erläutert.
Also welche Datentypen und eventuelle Restriktionen gefordert sind.

## 5. Fazit

### 5.1 Ausblick

Um die App nun Nutzern zur Verfügung zu stellen, sind vor der Veröffentlichung noch Themen zu Bedenken sowie essenzielle Funktionen in die App sowie das Backend einzubauen.

#### 5.1.1 Nutzerverwaltung

Essenziell wäre es eine Nutzerverwaltung hinzuzufügen.
Zum jetzigen Stand der App kann jeder Nutzer jedes Rezept bearbeiten und löschen.
Dies würde produktiv in einem einzigen Chaos enden. Daher muss gewährleistet sein, dass jeder Nutzer nur die von ihm selbst erstellten Rezepte bearbeiten und löschen kann.
Dies wäre schon mit einem einfachen System von Username & Password zu ermöglichen.
Dabei wäre es natürlich nicht mehr möglich sein Passwort beim Vergessen zurückzusetzen.
Speichert man stattdessen E-Mail oder Handynummer, müsste man diese erst bestätigen und produktiv ansprechen können.
Da die App derzeit sowieso nur auf Android Geräten zur Verfügung steht, wäre es aktuell am sinnvollsten dies über die `Google Play Developer API` zu lösen.
Dadurch müsste man auch keine Passwörter speichern und der Nutzer meldet sich über seinen (bereits mit Android verknüpften) Google Account an.
Die Oberfläche sowie das Backend wäre dahingehend zu erweitern.

#### 5.1.2 Google Play Store

Damit Nutzer die App herunterladen können, ist es am naheliegendsten die Software über den `Google Play Store` zu verteilen.
Dieser ist auf (fast) jedem Android Gerät vorinstalliert und hat dabei auch die größte Nutzerbasis.
Um dies tun zu können muss man sich zunächst in der `Google Play Console` anmelden und dort seinen Account durch eine einmalige Zahlung von 25$ freischalten.
Danach kann man eine App im PlayStore veröffentlichen.
Natürlich ist dabei auf die Google Richtlinien und AGBs zu achten.
So darf man unter anderem nur Apps oder Updates zur Verfügung stellen, welche die aktuellste Android Version unterstützen.
Zusätzlich sollte man sich weitere Gedanken um eine gute Präsentation der App im Store kümmern, dazu zählen Beschreibung, Tags und Bilder.

#### 5.1.3 Produktivsystem

Die CloudFoundry sowie die aktuelle Instanz können schon für einen Produktionsbetrieb genutzt werden.
Jedoch sollte man hier vor allem die Kapazität der Datenbank erweitern und eventuell mehrere Instanzen der Spring Boot Anwendung laufbar machen.
Fatal wäre es, sollte der Server überlastet sein, oder die Datenbank keinen Speicher mehr zur Verfügung haben.
Dadurch steigen selbstverständlich auch die laufenden Kosten zum Betrieb der App.
Hier wäre eine Kostenrechnung nötig um überschauen zu können mit welchen Fixkosten man beim Betrieb rechnen muss.

#### 5.1.4 Preisgestaltung

Durch die Fixkosten des Backends muss mit der App Geld verdient werden, um die Kosten zumindest decken zu können.
Dies könnte zum Beispiel über Werbung in der App oder über Verkäufe der App selbst realisiert werden.

Der vom Großteil der Android Nutzer angenommene Weg ist hierbei, sich auf Werbung zu konzentrieren und die App kostenlos anzubieten.
Werbung könnte man zum Beispiel zwischen den Rezepten in der Liste anzeigen oder am unteren Bildschirmrand.
Hier ist jedoch ein ausgewogenes Maß an Werbung zu beachten, da zu viel den Nutzer abschrecken könnte.
Werbung wäre mithilfe von `Google AdMob` zu realisieren.
In Kombination könnte man zusätzlich eine werbefreie Version der App anbieten, die jedoch einen kleinen Geldbetrag kostet.
Dies ist im `Google Play Store` auch eine gängige Methode.
Eine weitere Idee wäre es Interaktionen (zum Beispiel Anzahl an erstellbaren Rezepten) zu begrenzen und über In-App-Käufe zu erweitern.
Auch hier ist ein Frustfaktor für Nutzer zu beachten.

Bei Verkäufen der App selbst, von Abos, oder In-App-Käufen ist Googles 30% Anteil der Erlöse mitzuberechnen.
Des Weiteren sind auch Steuern auf die Einnahmen mit einzukalkulieren.
Letzten Endes wäre eine intensive Planung zur Finanzierung der App notwendig.

#### 5.1.5 AGB, Copyright sowie Moderation

Da unsere App auf Nutzerinhalte basiert, müssen diese überprüft und moderiert werden.
Dazu wäre der erste Schritt sich als Software Betreiber von den Inhalten der Nutzer über eine AGB rechtlich zu trennen.
Tut man dies nicht, wäre man als Unternehmen zum Beispiel für alle Copyright-Verletzungen auf hochgeladene Bilder verantwortlich.
Genauso ist über eine AGB dafür zu sorgen, dass der Nutzer seine eigenen Rechte für seine hochgeladenen Inhalte dem Softwarebetreiber freigibt.
Des Weiteren muss man sich ein Konzept zur Überwachung von Nutzer Inhalten überlegen.
Schließlich sollten die Bilder vom Essen und die Texte keine Beleidigungen enthalten.
Dafür könnte man neue Rezepte oder Änderungen vor der Veröffentlichung überprüfen lassen.
Dies kann ein bezahltes Team oder auch andere Nutzer übernehmen.
Dabei wäre letzteres anzupeilen, um eigene Kosten so gering wie möglich zu halten.
Mit einem `Trusted User`-System wie es zum Beispiel Wikipedia nutzt kann dies erreicht werden.

#### 5.1.6 Schnittstellen Updates

Beim Einbau neuer Features kann sich die Schnittstelle des Backends verändern.
Hier muss man eine Lösung finden, wie man mit Nutzern umgeht, die eine ältere App Version installiert haben und somit noch eine alte Schnittstelle ansprechen.

Eine Möglichkeit wäre es einen weiteren Endpoint einzubauen um die Aktualität der Schnittstelle überprüfen zu können.
Falls dann eine ältere Installation der App eine nun veränderte Schnittstelle ansprechen würde, wird diese nicht mehr gestartet.
Der Nutzer wird darauf hingewiesen die aktuellste Version der App zu installieren, um sie weiterhin verwenden zu können.
Dies könnte jedoch das Nutzererlebnis für Nutzer einschränken.
Eine andere Idee wäre es, alle Endpoints mit einer Versionsnummer zu versehen.
So würde sich zum Beispiel die Adresse zum Abrufen von einem Rezept in `/api/v1/recipes/id` ändern.
Jede Version der App schickt dann seine Anfragen an die passende Version des Endpoints.
Hier wäre jedoch der Wartungsaufwand für das Backend bei jedem Update höher, da weiterhin die korrekten Formate für ältere Endpoints benötigt werden würden.

Hier muss man das Nutzererlebnis und die Wartbarkeit abwägen.
Eventuell wäre auch eine Kombination beider Varianten die beste Lösung.
So werden für kleine Erweiterungen Schnittstellenversionierungen genutzt und für Breaking Changes (oder viel zu alte App Versionen) ein "deaktivieren" der App.
Dann können sehr alte Endpoints auch weggeschmissen werden.

### 5.2 Weitere denkbare Features

Aufgrund der beschränkten Zeit des Projekts und der Belastung, der anderen Fächer, konnte die App natürlich nicht bis ins kleinste Detail ausgearbeitet werden.
Das war ja auch nicht der Sinn, da es eher um die Organisation eines Projekts, die Planung, den Entwurf und um die gesammelten Erfahrungen ging.
An dieser Stelle möchten wir trotzdem noch auf ein paar Ideen eingehen, die für einen Release zu wünschen wären.

Ein Paging der Rezepteinträge wäre bei einem Produktivbetrieb noch einzuführen.
Bei unseren momentan 10-20 Rezepten macht die App keine Probleme beim Laden der Daten.
Sollten aber tausende Rezepte in der Datenbank liegen, dann wäre komplettes Laden der gesamten Liste viel zu langsam und man sollte ein Paging einführen.
Beispielsweise durch ein Seitenkonzept bei dem die nächste Seite erst nach dem Umschalten vom Server geladen wird oder ein Nachladen des nächsten Rezeptblocks, wenn der Nutzer den unteren Rand der Liste erreicht hat.

Ein Must-Have für eine Produktivnutzung ist eine Nutzerverwaltung.
Dazu müsste sich jeder Benutzer ein Konto erstellen und sich mit diesem Anmelden.
Die Rezepte wären so einem bestimmten Account zugeordnet und man könnte nur seine eigenen Rezepte editieren und löschen.
Aktuell könnte Nutzer einfach alle Rezepte löschen, was natürlich in Produktion nicht gewünscht ist.
Für die Benutzerverwaltung sind Security und Datenschutz-Richtlinien zu beachten und müssten entsprechend umgesetzt werden.  

Zudem wäre ein Suchfunktion in der App wünschenswert.
Bei einer Vielzahl an Rezepten ist ein durchscrollen aller Einträge sehr mühsam und aufwändig.
Daher sollte der Nutzer über eine Suchleiste nach bestimmten Stichworten suchen können und so ein gewünschtes Rezept finden.

Auch eine Favoritenspeicherung wäre ein gutes Feature.
Dazu kann der Nutzer gute Rezepte abspeichern, um diese später schneller wiederzufinden.
Dadurch gehen nachgekochte Rezepte nicht in der Liste verloren und sind für den Koch schnell einzusehen.

Wir möchten an dieser Stelle nicht auf alle Möglichkeiten eingehen.
Die App ist stark erweiterbar und sehr vielseitig.

Hier eine Liste an weiteren Ideen:

* Schwierigkeitsgrad, Zubereitungsdauer und Nähwertangaben für Rezepte
* Tags für die Suche
* Kommentar und Bewertungsfunktionen
* Einkaufszettel generieren
* Trends und "Rezept des Tages"
* Rezeptentwürfe abspeichern
* Skalieren der Zutaten an die Personenzahl
* Bilder für die einzelnen Zubereitungsschritte  

### 5.3 Beurteilung der Teamorganisation und des Vorgehensmodells

Die Teamorganisation hat unserer Meinung nach sehr gut funktioniert.
Für uns hat sich GitLab als gutes Organisations-Tool bewährt und wir haben sehr viele Funktionen für eine gute Zusammenarbeit genutzt.
Eine manuelle Absprache über WhatsApp war nur in sehr seltenen Fällen nötig.
Durch das digitale Board in GitLab konnte jeder den aktuellen Stand der Issues nachvollziehen und sich dementsprechend auf seine Aufgaben vorbereiten und diese durchführen.
Es war zu jeder Zeit für alle ersichtlich, wo das Team im Projektablauf steht und was gerade zu tun ist.

Durch unsere drei Dailys pro Woche konnten Probleme oder Unstimmigkeiten in sehr kurzer Zeit erkannt und angesprochen werden und ein frühzeitiges Eingreifen und Beheben war dadurch möglich.
Zusätzlich sind wir durch die häufigen Meetings, auch wenn wir uns teilweise noch nie persönlich gesehen haben und uns teilweise kaum kennen, zu einem guten Team zusammen gewachsen und konnten sehr offen miteinander reden.
Trotz der teilweise hohen Belastung durch andere Vorlesungen konnten wir uns durch die Treffen gegenseitig zum Arbeiten motivieren und so "Crunch-Times" weitestgehend vermeiden.  

Das Abnehmen der Issues im Team und das Durchführen von Code-Reviews haben den Teamzusammenhalt und die Teamdynamik zusätzlich gestärkt.
Bei den Reviews konnten wir uns gegenseitig bei Problemen helfen oder Tipps geben, um so unser Wissen untereinander zu teilen und das bestmögliche Ergebnis zu erzielen.
Zudem konnten wir beim Abnehmen der Issues in unseren Meetings den Fortschritt unseres Produkts sofort alle sehen, was zu einer Motivation des gesamten Teams beitrug.

Die Moderation hätte man eventuell noch besser lösen können.
Es waren beispielsweise immer dieselben Moderatoren in den Dailys, die das Gespräch geleitet und geführt haben.
Das führte zwar zu keinerlei Problemen, aber eine rotierende Moderatorenrolle wäre bestimmt eine gute Alternative gewesen.  

Alles in allem sind wir mit unserem Vorgehen sehr zufrieden und wir denken für diese Art von Projekt war unsere Vorgehensweise gut geeignet.
Wir konnten die agile Entwicklung, angepasst an den Projektumfang, sehr gut umsetzen und konnten uns einen guten Einblick in die Praxis-Umsetzung des Software Engineerings verschaffen.

## Glossar

Wort | Bedeutung
---- | ---------
Cloud Foundry | Cloud Foundry stellt sicher, dass die Build- und Bereitstellungsaspekte bei der Codierung sorgfältig mit allen verbundenen Services koordiniert werden. Dies bewirkt eine schnelle, konsistente und zuverlässige Iteration von Anwendungen.
REST-Api | REST-API macht den Austausch von Informationen möglich, wenn diese sich auf unterschiedlichen Systemen befinden.
test-driven | Test Driven ist eine Designstrategie, die das Testen vor dem Erstellen des Quellcodes ansiedelt und mit Bezug auf die Abläufe vorrangig behandelt. Das Ziel liegt darin, die Qualität der Software maßgeblich zu erhöhen und den Wartungsaufwand im Nachhinein zu verringern.
Unit Tests | Unit Tests überprüfen auf technischer Ebene, ob der von den Software Entwicklern geschriebener Code-Fragment ordnungsgemäß arbeitet.
CI/CD-Pipeline | CI/CD steht für eine Sammlung von Techniken, Prozessen und Werkzeugen, um die Softwareentwicklung und -auslieferung zu verbessern. Man spricht meist von einer CI/CD-Pipeline, da es sich um einen aufeinander aufbauenden, aber in sich geschlossenen Feedback-Prozess handelt.
Deployment | Bereitstellung von Software
Refactor | Refactoring (auch Refaktorisierung, Refaktorierung oder Restrukturierung) bezeichnet in der Software-Entwicklung die manuelle oder automatisierte Strukturverbesserung von Quelltexten unter Beibehaltung des beobachtbaren Programmverhaltens.
Swagger | Swagger ist eine Sammlung von Open-Source-Werkzeugen, um HTTP-Webservices zu entwerfen, zu erstellen, zu dokumentieren und zu nutzen.

## Anhang

[Zu unseren GitLab-Projekten hier klicken](https://gitlab.com/lets-cook)
