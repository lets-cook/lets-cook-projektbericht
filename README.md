Der Bericht ist eine PDF-Datei mit den folgenden Inhalten, die als einzelne Kapitel verstanden werden können:

(1) Deckblatt mit Titel, alle vollständige Namen, E-Mail-Adressen und Matrikelnummern

(2) Inhaltsangabe, d. h. Kapitelstruktur des Dokuments

(3) Kurzbeschreibung des Projektziels

(4) Nennung der Aufgabenbereiche jedes Teammitglieds

(5) Priorisierte Anforderungsliste in Textform, zeitlich aufgeteilt (in unterschiedliche Abschnitte) einmal nach dem Meilenstein „Alpha“ (s.u.), und dann nach dem Termin der Abschlusspräsentation (s.u.)

(6) Soll-Ist-Auswertung aller (!) ursprünglich formulierten Anforderungen, z. B. als Markierung des Realisierungszeitpunkts (Meilenstein „Alpha“ oder „Abschluss“(-Präsentation))

(7) Verweis auf verwendete Quellcode-Konventionen (bzw. eigene Beschreibung dieser)

(8) Beschreibung der durchgeführten Tests


Achtung: Das Praktikum (sowohl der Bericht als auch die Abschlusspräsentation) dient in diesem Semester als Grundlage für die Benotung der Veranstaltung Software Engineering (es findet keine Klausur statt). Aus diesem Grund sind zusätzliche Fragen in dem Projektbericht zu beantworten:


(9) Überführung in die Praxis: wie würde die entwickelte Software nun in der Praxis bereitgestellt werden, wie würde die Wartung sichergestellt werden?

(10) Welche zusätzlichen Aspekte sind bei dem Projekt in der Praxis relevant? (bspw. Sicherheit/Datenschutz, Performanz, etc.) - und wie kann dies umgesetzt werden?

(11) Welches Vorgehensmodell wurde gewählt und warum? (welche Erfahrung hat das Team damit gemacht?)

(12) Wie wurde die Team-/Projektorganisation umgesetzt? (mit welchen Tools, was hat gut/schlecht funktioniert?)
    
a. Zeitpläne, Scrum-Boards, etc. sollen im Bericht mit aufgenommen werden

(13) Es sollen alle vorgestellten UML-Diagramme genutzt und in dem Bericht zu finden sein.


Bei der Erstellung der Anforderungsliste berücksichtigen Sie beide Aspekte, funktionale und nicht-funktionale Anforderungen. Beschreiben Sie diese sauber (nach einer in der Vorlesung vorgestellten Methode). Verworfene Anforderungen, bzw. die nicht mehr realisiert werden können, sollen nicht aus dem Dokument gelöscht werden! Stattdessen markieren Sie nur diese entsprechend!


Der Quellcode sollte in einer ZIP-Datei zusammengefasst werden und nur (!) die tatsächlich eigens entwickelten bzw. erstellten Beiträge enthalten (kein „externer“ Code). Beachten Sie, dass die Implementierung auf konsistenten Quellcode-Konventionen basieren sollte. Der Quellcode sollte durchgängig und sinnvoll kommentiert sein, so dass er für „Dritte“ verständlich ist.